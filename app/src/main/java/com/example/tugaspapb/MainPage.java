package com.example.tugaspapb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        TextView textViewUsername = findViewById(R.id.displayUsername);
        TextView textViewPassword = findViewById(R.id.displayPassword);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            String username = extras.getString("username");
            String password = extras.getString("password");

            textViewUsername.setText("Username : " + username);
            textViewPassword.setText("Password : " + password);
        }
    }
}