package com.example.tugaspapb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.buttonLogin);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText inputUname = findViewById(R.id.inputUsername);
                EditText inputPass = findViewById(R.id.inputPassword);

                String username = inputUname.getText().toString();
                String password = inputPass.getText().toString();

                if (loginValidate(username, password)){
                    Intent intent = new Intent(MainActivity.this, MainPage.class);
                    intent.putExtra("username", username);
                    intent.putExtra("password", password);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Invalid username or password", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean loginValidate(String username, String password){
        return (username.equals("Rizka") && password.equals("047")) || (username.equals("icha") && password.equals("123"));
    }

}